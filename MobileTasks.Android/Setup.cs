﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MobileTasks.Core;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;

namespace MobileTasks.Android
{
    public class Setup : MvxAndroidSetup
    {
        protected Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }
    }
}