﻿using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileTasks.Core.Base
{
    public class BaseViewModel<T, RetT> : MvxViewModel<T, RetT> where T : class where RetT : class
    {
        private IMvxNavigationService _navigationService;

        private T _parameter;
        private RetT _returnValue;

        public BaseViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void Prepare(T parameter)
        {
            _parameter = parameter;
        }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        private IMvxCommand _closeCommand;
        public virtual IMvxCommand CloseCommand => _closeCommand = _closeCommand ?? new MvxCommand(() => 
        {
            _navigationService.Close(this);
        });
    }
}
