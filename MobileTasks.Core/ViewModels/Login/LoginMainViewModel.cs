﻿using MobileTasks.Core.Base;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobileTasks.Core.ViewModels.Login
{
    public class LoginMainViewModel : BaseViewModel<Object, Object>
    {
        public LoginMainViewModel(IMvxNavigationService navigationService) : base(navigationService)
        {

        }

        public override void Prepare(Object parameter)
        {
            base.Prepare(parameter);

        }

        public override Task Initialize()
        {
            return base.Initialize();

        }

        private IMvxCommand _showLoginBoardCommand;
        public IMvxCommand ShowLoginBoardCommand => _showLoginBoardCommand = _showLoginBoardCommand ?? new MvxCommand(() =>
        {
            
        });
    }
}
