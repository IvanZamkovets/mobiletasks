﻿using MobileTasks.Core.Base;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobileTasks.Core.ViewModels.Login
{
    public class LoginViewModel : BaseViewModel<object, object>
    {


        public LoginViewModel(IMvxNavigationService navigationService) : base(navigationService)
        {
        }

        private IMvxCommand _signInCommand;
        public IMvxCommand SignInCommand => _signInCommand = _signInCommand ?? new MvxCommand(() =>
        {

        });
        private IMvxCommand _signUpCommand;
        public IMvxCommand SignUpCommand => _signUpCommand = _signUpCommand ?? new MvxCommand(() =>
        {

        });
    }
}
